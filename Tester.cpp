#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <Test.hpp>
#include <DBDummy.hpp>
#include <MySQL.hpp>

using namespace std;

//Assume a command line call:
//./Tester [N - num of flights] [DB choice] 
int main(int argc, char* argv[]){
    DBI* dut = NULL;
    std::string dbName;
    long size = 0;
    
    if(argc < 2){
        cout << "Need to specify a size, and optionally a DB" << endl;
        return -1;
    } else {
        size = atol(argv[1]);
    }
    
    //No DB choice check
    if(argc < 3){
        dut = new DBDummy();
        dbName = "Dummy";
    } else {
        string input = argv[2];
        if(input == "MySQL"){
            dut = new MySQL();
            dbName = "MySQL";
        } else if(input == "Mongo"){

        } else {
            dut = new DBDummy();
            dbName = "Dummy";
        }
    }
    
    //Iterate over tests
    float increment = 0.01;
    const float limit = 1.0;
    for(float read = 0.0; read <= limit; read+=increment){
        Test *t = new Test(size,read,dut);
        long seconds;
        long ops;
        
        if(!t->runTest(seconds,ops)){
            cout << "Test failed" << endl;
        }
        
        cout << dbName << "," << size << "," << seconds << "," << setprecision(3) << read << endl;

        delete t;
    }
    
    return 0;
}
