#include <catch.hpp>
#include <Test.hpp>
#include <DBDummy.hpp>

TEST_CASE("Able to run a test","[Test]"){
    Test first(1000000, 0.35, new DBDummy());
    long seconds, ops;

    REQUIRE(first.runTest(seconds,ops) == true);

    REQUIRE(ops == 1000000);
}
