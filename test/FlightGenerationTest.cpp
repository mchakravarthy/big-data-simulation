#include <catch.hpp>
#include <FlightGenerator.hpp>
#include <Flight.hpp>
#include <map>

namespace {
    std::string concatFlight(Flight in){
        std::string flightNumber = static_cast<std::ostringstream*>(&(std::ostringstream()<<in.flightNum))->str();
        return in.carrier += flightNumber;
    }
      
    bool checkUnique(const FlightList& input){
        std::map<std::string, int> unique;
        
        FlightConstIter iter;
        std::map<std::string, int>::iterator mapIter;
        for(iter = input.begin(); iter != input.end(); iter++){
            std::string flightStr = concatFlight(*iter);
            mapIter = unique.find(flightStr);
            
            //Insert if not found
            if(mapIter == unique.end()){
                unique.insert(std::pair<std::string,int>(flightStr,1));
            } else {
                return false;
            }
        }
        
        //Passes if escapes loop
        return true;
        
    }
}

TEST_CASE("Test that it creates 1,000,000 unique flights", "[FlightGenerator]"){
    FlightGenerator gen;
    FlightList result;
     
    
    gen.generateFlights(1000000, result);
    
    REQUIRE(result.size() == 1000000);
    REQUIRE(checkUnique(result) == true);
}

TEST_CASE("Able to split flights correctly", "[FlightGenerator]"){
    FlightGenerator gen;
    FlightList result, reads, writes;
     
    
    gen.generateFlights(1000000, result);
    
    REQUIRE(result.size() == 1000000);
    REQUIRE(checkUnique(result) == true);
    
    float rw = 0.5;
    gen.splitFlights(result, rw, reads, writes);
    
    REQUIRE(reads.size() == 500000);
    REQUIRE(checkUnique(result) == true);
    
    REQUIRE(writes.size() == 500000);
    REQUIRE(checkUnique(result) == true);
}

