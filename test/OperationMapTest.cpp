#include <catch.hpp>
#include <OperationMap.hpp>
#include <FlightGenerator.hpp>
#include <Flight.hpp>

TEST_CASE("Check getNextRemaining and getNextOperation work", "[OperationMap]"){
    FlightGenerator gen;
    FlightList result, reads, writes;
    
    gen.generateFlights(1000000, result);
    
    float rw = 0.5;
    gen.splitFlights(result, rw, reads, writes);
    
    OperationMap dut(reads,writes);
    
    REQUIRE(dut.getRemaining() == 1000000);
    
    Flight out;
    bool read = dut.getNextOperation(out);
    
    REQUIRE(dut.getRemaining() == 999999);
}
