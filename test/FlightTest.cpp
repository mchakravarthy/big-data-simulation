#include <catch.hpp>
#include <Flight.hpp>

TEST_CASE("Testing Flight data creation", "[Flight]"){
    Flight a("QF", 1234, 100, 100, "To carry people");
    
    REQUIRE(a.carrier == "QF");
    REQUIRE(a.flightNum == 1234);
    REQUIRE(a.numPassengers == 100);
    REQUIRE(a.flightWeight == 100);
    REQUIRE(a.purpose == "To carry people");
}