CC=g++ -I lib/ -I src/ -I test/ -std=c++11

MYSQL_FLAGS = -L/usr/include/mysql -lmysqlclient -I/usr/include/mysql

HEADERS = src/*.hpp

SOURCES = src/*.cpp

TESTS = test/*.cpp

HEADER_LIBRARIES = lib/*.hpp

SRC_LIBRARIES = lib/*.cpp

all: MainTest Tester

clean: 
	rm MainTest

# Test makes
MainTest: mainTest.cpp $(TESTS) $(SOURCES)
	$(CC) $(HEADER_LIBRARIES) $(HEADERS) $(SOURCES) $(TESTS) mainTest.cpp -o $@
	
# Application makes
Tester: Tester.cpp $(SOURCES)
	$(CC) $(HEADERS) $(SOURCES) Tester.cpp -o $@ $(MYSQL_FLAGS)
	

