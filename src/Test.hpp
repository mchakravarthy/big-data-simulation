#ifndef TEST_HEADER
#define TEST_HEADER

//This class represents a single test

#include <DBI.hpp>

class Test{
    private:
        long N;
        float readPercent;
        DBI* driver;
        
    public:
        Test(long size, float read, DBI* db);
        
        //Returns in milliseconds how long the test took
        //and how many operations were performed
        bool runTest(long& seconds, long& ops);
};

#endif
