#include <OperationMap.hpp>
#include <algorithm>


OperationMap::OperationMap(FlightList reads, FlightList writes){
    //Populate vector with reads
    FlightIter readIter;
    for(readIter = reads.begin(); readIter != reads.end(); ++readIter){
        std::pair<bool,Flight> readInsert(true,*readIter);
        _ops.push_back(readInsert);
    }
    
    //Populate vector with writes
    FlightIter writeIter;
    for(writeIter = writes.begin(); writeIter != writes.end(); ++writeIter){
        std::pair<bool,Flight> writeInsert(false,*writeIter);
        _ops.push_back(writeInsert);
    }
    
    std::random_shuffle(_ops.begin(), _ops.end());

    _size = reads.size() + writes.size();
    _current = 0;
    
}

bool OperationMap::getNextOperation(Flight& out){
    std::pair<bool,Flight> cur = _ops[_current];
    
    _current++;
    
    out = cur.second;
    
    return cur.first;
    
}

long OperationMap::getRemaining(){
    return _size - _current;
}
