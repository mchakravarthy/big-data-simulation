#ifndef OP_HEADER
#define OP_HEADER

#include <Flight.hpp>
#include <vector>

//This class maps an operation (read/write) to a Flight

class OperationMap {
   private:
   
   std::vector< std::pair<bool,Flight> > _ops;
   long _size;
   long _current;
   
   public:
   
   //Constructs the operation map from read and writes
   //Then performs random shuffling on the operations
   OperationMap(FlightList reads, FlightList writes);
   
   //Returns if it is a read operation or not
   //and the relevant flight in reference
   bool getNextOperation(Flight& out);
   
   //Returns number of remaining operations for iteration purposes
   long getRemaining();
   
};

#endif