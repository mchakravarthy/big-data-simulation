#include <FlightGenerator.hpp>
#include <cmath>

bool FlightGenerator::generateFlights(const long& n, FlightList& oFlight){    
    for(long i = 0; i < n; i++){
        
        std::string carrier = "";
        int flightNum;
        
        generateFlight(i,carrier,flightNum);
        
        Flight in(carrier,flightNum,100,55000.6,"To carry people");
        
        oFlight.push_back(in);
    }
}

void FlightGenerator::generateFlight(const long &i, std::string& carrier, int& flight){
    char first = (i/260000) + 65;
    char second = (i/10000) + 65;
    
    carrier += first;
    carrier += second;
    
    flight = i % 10000;
}

void FlightGenerator::splitFlights(FlightList& iData, float& readPercent, 
                                   FlightList& oReads, FlightList& oWrites){
    double size = (double)iData.size();
    double read = (double) readPercent;
    long turningPt = (long) std::floor(read * size);
    
    long counter = 0;
    
    FlightIter dataIter;
    
    for(dataIter = iData.begin(); dataIter != iData.end(); ++dataIter){
        Flight input = (*dataIter);
        if(counter < turningPt){
            oReads.push_back(input);
        } else {
            oWrites.push_back(input);
        }

        counter++;
    }
}
