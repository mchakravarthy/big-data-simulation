#ifndef FLIGHT_HEADER
#define FLIGHT_HEADER 

//This class constitutes the individual data item upon which this simulation is based.
//To make it slightly themed, we're using flights, as this seems like a big data application at airports
//but who knows.

#include <list>
#include <string>

class Flight {
    public:
        Flight(){}
        std::string carrier; //Carrier e.g. QF, JL, etc
        int flightNum; //Flight number like 685, 8023
        int numPassengers; //Number of passengers onboard
        double flightWeight; //Weight of plane with passengers
        std::string purpose; //Purpose of the flight
        Flight(std::string c, int f, int num, double weight, std::string p){
            carrier = c;
            flightNum = f;
            numPassengers = num;
            flightWeight = weight;
            purpose = p;
        }
};

typedef std::list<Flight> FlightList;

typedef std::list<Flight>::iterator FlightIter;

typedef std::list<Flight>::const_iterator FlightConstIter;

#endif
