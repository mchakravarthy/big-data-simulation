#ifndef MYSQL_HEADER
#define MYSQL_HEADER

#include <DBI.hpp>
#include <mysql.h>

class MySQL : public DBI {
   private:
   MYSQL* connect;

   public:
   MySQL();

   void initialiseDB(const FlightList& flights);
    
   bool readFlight(const std::string& carrier, const int& flightNum);
    
   void writeFlight(const Flight& iFlight);
};

#endif
