#ifndef DUMMY_HEADER
#define DUMMY_HEADER

#include <Flight.hpp>
#include <DBI.hpp>

class DBDummy : public DBI {
    public:
    void initialiseDB(const FlightList& flights){
        return;
    }
    
    bool readFlight(const std::string& carrier, const int& flightNum){
        return true;
    }
    
    void writeFlight(const Flight& iFlight){
        return;
    }

};

#endif