#include <MySQL.hpp>
#include <sstream>

MySQL::MySQL(){
   connect = mysql_init(NULL);
   connect = mysql_real_connect(connect,"localhost","root","mchak93","sim",0,NULL,0);
}

void MySQL::initialiseDB(const FlightList& flights){
   //Refreshes the Database
   mysql_query(connect,"DROP TABLE flights;");
   mysql_query(connect,"CREATE TABLE flights(carrier varchar(2),flightNumber int,numPassengers int,flightWeight double,purpose varchar(255));");

   FlightConstIter iter;

   for(iter = flights.begin(); iter != flights.end(); ++iter){
      Flight in = *(iter);
      writeFlight(in);
   }
}
    
bool MySQL::readFlight(const std::string& carrier, const int& flightNum){
   std::stringstream query;

   query << "SELECT * FROM flights WHERE ";

   query << "carrier=" << carrier;

   query << " AND flightNum=" << flightNum;

   mysql_query(connect,query.str().c_str());

   MYSQL_RES *res_set = NULL;
   res_set = mysql_store_result(connect);

   if(res_set != NULL){
      unsigned int numrows = mysql_num_rows(res_set);
      if(numrows > 0){
         return true;
      } else {
         return false;
      }
   } else {
      return false;
   }
}
    
void MySQL::writeFlight(const Flight& iFlight){
   std::stringstream query;

   query << "INSERT INTO flights(carrier,flightNumber,numPassengers,flightWeight,purpose)";

   query << "VALUES(";
   query << "'" << iFlight.carrier << "'";
   query << ",";
   query << "'" << iFlight.flightNum << "'";
   query << ",";
   query << "'" << iFlight.numPassengers << "'";
   query << ",";
   query << "'" << iFlight.flightWeight << "'";
   query << ",";
   query << "'" << iFlight.purpose << "'";
   query << ");";

   mysql_query(connect,query.str().c_str());
}
