#ifndef DBI_HEADER
#define DBI_HEADER

//This class acts as an adapter to communicate with different DBs. 
//For a DB to be tested using this system, it has to implement this interface.

#include <Flight.hpp>

class DBI {
    public:
    virtual void initialiseDB(const FlightList& flights) = 0;
    
    virtual bool readFlight(const std::string& carrier, const int& flightNum) = 0;
    
    virtual void writeFlight(const Flight& iFlight) = 0;
};

#endif