#include <Test.hpp>
#include <FlightGenerator.hpp>
#include <Flight.hpp>
#include <OperationMap.hpp>
#include <ctime>
#include <chrono>

Test::Test(long size, float read, DBI* db){
    N = size;
    readPercent = read;
    driver = db;
}

bool Test::runTest(long& seconds, long& ops){
    seconds = 0;
    ops = 0;

    //Generate list of flights
    FlightGenerator flightGen;
    FlightList data;
    
    flightGen.generateFlights(N,data);
    
    //Split list of flights into reads and writes
    FlightList reads;
    FlightList writes;
    
    flightGen.splitFlights(data,readPercent,reads,writes);
    
    //Initialise the DB schema with read flights
    try{
        driver->initialiseDB(reads);
    } catch (std::exception& e){
        return false;
    }
    
    //Construct operations map and shuffle
    OperationMap opMap(reads,writes);
    
    //Start timer
    auto t_start = std::chrono::high_resolution_clock::now();
    
    //Run each operations
    try {
        while(opMap.getRemaining() > 0){
            Flight toUse;
            if(opMap.getNextOperation(toUse)){
                driver->readFlight(toUse.carrier, toUse.flightNum);
            } else {
                driver->writeFlight(toUse);
            }
        }
    } catch (std::exception& e){
        return false;
    }
    
    //Stop timer
    auto t_end = std::chrono::high_resolution_clock::now();
    
    //Return results
    ops = N;
    seconds = (long)std::chrono::duration<double, std::milli>(t_end-t_start).count();

    return true;
}
