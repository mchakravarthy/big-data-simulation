#ifndef FLIGHT_GEN_HEADER
#define FLIGHT_GEN_HEADER

//This class, given an N, 
//will generate that many Flight objects, 
//all of which are unique

//Also provided a read percentage,
//will split the flights into reads and writes

#include <Flight.hpp>

class FlightGenerator{
    private:
        void generateFlight(const long &i, std::string& carrier, int& flight);
    public:
        bool generateFlights(const long& n, FlightList& oFlight);
        void splitFlights(FlightList& iData, float& readPercent, FlightList& oReads, FlightList& oWrites);
        
};

#endif